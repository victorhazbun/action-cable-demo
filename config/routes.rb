Rails.application.routes.draw do


  mount ActionCable.server => '/cable'

  get 'spreadsheet/index'


  root 'spreadsheet#index'
end
